#!/bin/make -f
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#   * GNU on Windows NT (using MinGW/MSYS/Cygwin/WSL)
#
# Build:
#   make [options]
# Install / Uninstall:
#   make [options] install | uninstall
# Clean:
#   make [options] clean | distclean
#
# Options:
#   INCLUDEDIR=<dir>
#     <dir> is a directory where "lodepng.h" file is.
#   LIBDIR=<dir>
#     <dir> is a directory where compiled "liblodepng" library is.
#   STATIC_BUILD=<FLAG>
#     <FLAG> - 0 or 1
#   BUILD=<BUILD>
#     <BUILD> - see included `common.mak'.
#   prefix=<PREFIX>
#     <PREFIX> is a prefix directory to install files into.
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
# SPDX-License-Identifier: GPL-3.0-or-later

include common.mak
-include conf.mak

# 0 or 1
STATIC_BUILD ?= 0

ifeq ($(STATIC_BUILD),0)
 builddir	= build
else
 builddir	= build/static
endif

prefix		?= /usr/local
exec_prefix	?= $(prefix)
bindir		?= $(exec_prefix)/bin
libdir		?= $(exec_prefix)/lib

INCLUDEDIR	?= lodepng
LIBDIR		?= build/lib

VERSION := $(shell cat VERSION)

LINUX_I686_DYNAMIC  = therighttools-$(VERSION)-linux-i686.tar.bz2
LINUX_I686_STATIC   = therighttools-$(VERSION)-linux-i686-static.tar.bz2
LINUX_AMD64_DYNAMIC = therighttools-$(VERSION)-linux-amd64.tar.bz2
LINUX_AMD64_STATIC  = therighttools-$(VERSION)-linux-amd64-static.tar.bz2

WINDOWS_I686_DYNAMIC  = therighttools-$(VERSION)-windows-i686.zip
WINDOWS_I686_STATIC   = therighttools-$(VERSION)-windows-i686-static.zip
WINDOWS_AMD64_DYNAMIC = therighttools-$(VERSION)-windows-amd64.zip
WINDOWS_AMD64_STATIC  = therighttools-$(VERSION)-windows-amd64-static.zip

.PHONY: all
all: build-lodepng build-tools

build \
build/linux-i686 \
build/linux-i686-static \
build/linux-amd64 \
build/linux-amd64-static \
build/windows-i686 \
build/windows-i686-static \
build/windows-amd64 \
build/windows-amd64-static \
$(DESTDIR)$(bindir) \
$(DESTDIR)$(libdir):
	mkdir -p $@

#-----------------------------------------------------------------------------
# LodePNG

ifeq ($(STATIC_BUILD),0)

.PHONY: build-lodepng
build-lodepng: | lodepng
	$(MAKE) -w -C $| prefix=../build install

.PHONY: install-lodepng
install-lodepng: | $(DESTDIR)$(libdir)
	$(INSTALL_PROGRAM) -m 644\
	 build/lib/liblodepng$(DLLSUFFIX)\
	 $(DESTDIR)$(libdir)/liblodepng$(DLLSUFFIX)

.PHONY: uninstall-lodepng
uninstall-lodepng:
	$(RM) $(DESTDIR)$(libdir)/liblodepng$(DLLSUFFIX)

.PHONY: clean-built-lodepng
clean-built-lodepng:
	$(RM) build/lib/liblodepng$(DLLSUFFIX)

.PHONY: clean-lodepng
clean-lodepng: clean-built-lodepng | lodepng
	$(MAKE) -w -C $| clean

.PHONY: distclean-lodepng
distclean-lodepng: clean-built-lodepng | lodepng
	$(MAKE) -w -C $| distclean

else	# $(STATIC_BUILD)!=0

.PHONY:\
 build-lodepng\
 install-lodepng\
 uninstall-lodepng\
 clean-lodepng\
 distclean-lodepng

build-lodepng \
install-lodepng \
uninstall-lodepng \
clean-lodepng \
distclean-lodepng:;

endif	# $(STATIC_BUILD)!=0

#-----------------------------------------------------------------------------
# Tools

.PHONY: build-tools
build-tools: | src
ifeq ($(STATIC_BUILD),0)
	$(MAKE) -w -C $|\
	 INCLUDEDIR=$(realpath $(INCLUDEDIR))\
	 LIBDIR=$(realpath $(LIBDIR))\
	 prefix=../$(builddir)\
	 install
else
	$(MAKE) -w -C $|\
	 INCLUDEDIR=$(realpath $(INCLUDEDIR))\
	 prefix=../$(builddir)\
	 install
endif

$(DESTDIR)$(bindir)/%$(EXESUFFIX): $(builddir)/bin/%$(EXESUFFIX)\
 | $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) $< $@

.PHONY: install-tools
install-tools:\
 $(DESTDIR)$(bindir)/fontconv$(EXESUFFIX)\
 $(DESTDIR)$(bindir)/scrconv$(EXESUFFIX)

.PHONY: uninstall-tools
uninstall-tools:
	$(RM)\
	 $(DESTDIR)$(bindir)/fontconv$(EXESUFFIX)\
	 $(DESTDIR)$(bindir)/scrconv$(EXESUFFIX)

.PHONY: clean-built-tools
clean-built-tools:
	$(RM)\
	 $(builddir)/bin/fontconv$(EXESUFFIX)\
	 $(builddir)/bin/scrconv$(EXESUFFIX)

.PHONY: clean-tools
clean-tools: clean-built-tools | src
	$(MAKE) -w -C $| clean

.PHONY: distclean-tools
distclean-tools: clean-built-tools | src
	$(MAKE) -w -C $| distclean

#-----------------------------------------------------------------------------
# Release: Linux

ifneq ($(RELEASE_LINUX_DYNAMIC),)
build/$(RELEASE_LINUX_DYNAMIC): | lodepng src
	$(MAKE) -w -C lodepng\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(MAKE) -w -C src\
	 INCLUDEDIR=../lodepng\
	 LIBDIR=../build/$(RELEASE_SUBDIR)/lib\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(RM) $@ && cd build/$(RELEASE_SUBDIR) &&\
	 tar --owner= --group= -cjf ../$(@F)\
	 lib/liblodepng.so\
	 bin/fontconv\
	 bin/scrconv
endif

ifneq ($(RELEASE_LINUX_STATIC),)
build/$(RELEASE_LINUX_STATIC): | lodepng src
	$(MAKE) -w -C src\
	 INCLUDEDIR=../lodepng\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(RM) $@ && cd build/$(RELEASE_SUBDIR) &&\
	 tar --owner= --group= -cjf ../$(@F)\
	 bin/fontconv\
	 bin/scrconv
endif

#-----------------------------------------------------------------------------
# Release: Windows

ifneq ($(RELEASE_WINDOWS_DYNAMIC),)
build/$(RELEASE_WINDOWS_DYNAMIC): | lodepng src
	$(MAKE) -w -C lodepng\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(MAKE) -w -C src\
	 INCLUDEDIR=../lodepng\
	 LIBDIR=../build/$(RELEASE_SUBDIR)/lib\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(RM) $@ && cd build/$(RELEASE_SUBDIR) && chmod +x lib/*.dll &&\
	 zip -r9 ../$(@F)\
	 lib/liblodepng.dll\
	 bin/fontconv.exe\
	 bin/scrconv.exe
endif

ifneq ($(RELEASE_WINDOWS_STATIC),)
build/$(RELEASE_WINDOWS_STATIC): | lodepng src
	$(MAKE) -w -C src\
	 INCLUDEDIR=../lodepng\
	 builddir=build/$(RELEASE_SUBDIR)\
	 prefix=../build/$(RELEASE_SUBDIR)\
	 install-strip
	$(RM) $@ && cd build/$(RELEASE_SUBDIR) &&\
	 zip -r9 ../$(@F)\
	 bin/fontconv.exe\
	 bin/scrconv.exe
endif

#-----------------------------------------------------------------------------
# Release

.PHONY: release
release: | build
	echo -n '' >build/release.md
 ifneq ($(OS),Windows_NT)
	{ echo '**GNU/Linux x86** platform:';\
	 echo '';\
	 echo 'Architecture | Build | File | SHA-256';\
	 echo '---|---|---|---'; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=0 CFLAGS='-m32 $(CFLAGS)'\
	 RELEASE_SUBDIR=linux-i686\
	 RELEASE_LINUX_DYNAMIC=$(LINUX_I686_DYNAMIC)\
	 build/$(LINUX_I686_DYNAMIC)
	{ echo -n '32-bits (i686) | dynamic | $(LINUX_I686_DYNAMIC) | ';\
	 sha256sum build/$(LINUX_I686_DYNAMIC) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=0 CFLAGS='-m64 $(CFLAGS)'\
	 RELEASE_SUBDIR=linux-amd64\
	 RELEASE_LINUX_DYNAMIC=$(LINUX_AMD64_DYNAMIC)\
	 build/$(LINUX_AMD64_DYNAMIC)
	{ echo -n '64-bits (AMD64) | dynamic | $(LINUX_AMD64_DYNAMIC) | ';\
	 sha256sum build/$(LINUX_AMD64_DYNAMIC) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=1 CFLAGS='-m32 $(CFLAGS)'\
	 RELEASE_SUBDIR=linux-i686-static\
	 RELEASE_LINUX_STATIC=$(LINUX_I686_STATIC)\
	 build/$(LINUX_I686_STATIC)
	{ echo -n '32-bits (i686) | static | $(LINUX_I686_STATIC) | ';\
	 sha256sum build/$(LINUX_I686_STATIC) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=1 CFLAGS='-m64 $(CFLAGS)'\
	 RELEASE_SUBDIR=linux-amd64-static\
	 RELEASE_LINUX_STATIC=$(LINUX_AMD64_STATIC)\
	 build/$(LINUX_AMD64_STATIC)
	{ echo -n '64-bits (AMD64) | static | $(LINUX_AMD64_STATIC) | ';\
	 sha256sum build/$(LINUX_AMD64_STATIC) | cut -d\  -f 1; } >>build/release.md
	echo ''>> build/release.md
 endif
	{ echo '**Windows x86** platform:';\
	 echo '';\
	 echo 'Architecture | Build | File | SHA-256';\
	 echo '---|---|---|---'; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=0 BUILD=mingw32\
	 RELEASE_SUBDIR=windows-i686\
	 RELEASE_WINDOWS_DYNAMIC=$(WINDOWS_I686_DYNAMIC)\
	 build/$(WINDOWS_I686_DYNAMIC)
	{ echo -n '32-bits (i686) | dynamic | $(WINDOWS_I686_DYNAMIC) | ';\
	 sha256sum build/$(WINDOWS_I686_DYNAMIC) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=0 BUILD=mingw64\
	 RELEASE_SUBDIR=windows-amd64\
	 RELEASE_WINDOWS_DYNAMIC=$(WINDOWS_AMD64_DYNAMIC)\
	 build/$(WINDOWS_AMD64_DYNAMIC)
	{ echo -n '64-bits (AMD64) | dynamic | $(WINDOWS_AMD64_DYNAMIC) | ';\
	 sha256sum build/$(WINDOWS_AMD64_DYNAMIC) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=1 BUILD=mingw32\
	 RELEASE_SUBDIR=windows-i686-static\
	 RELEASE_WINDOWS_STATIC=$(WINDOWS_I686_STATIC)\
	 build/$(WINDOWS_I686_STATIC)
	{ echo -n '32-bits (i686) | static | $(WINDOWS_I686_STATIC) | ';\
	 sha256sum build/$(WINDOWS_I686_STATIC) | cut -d\  -f 1; } >>build/release.md
	$(MAKE) -w STATIC_BUILD=1 BUILD=mingw64\
	 RELEASE_SUBDIR=windows-amd64-static\
	 RELEASE_WINDOWS_STATIC=$(WINDOWS_AMD64_STATIC)\
	 build/$(WINDOWS_AMD64_STATIC)
	{ echo -n '64-bits (AMD64) | static | $(WINDOWS_AMD64_STATIC) | ';\
	 sha256sum build/$(WINDOWS_AMD64_STATIC) | cut -d\  -f 1; } >>build/release.md

.PHONY: clean-built-release
clean-built-release:
	$(RM) build/release.md
 ifneq ($(OS),Windows_NT)
	$(RM) -r\
	 build/linux-i686\
	 build/linux-i686-static\
	 build/linux-amd64\
	 build/linux-amd64-static
	$(RM)\
	 build/$(LINUX_I686_DYNAMIC)\
	 build/$(LINUX_I686_STATIC)\
	 build/$(LINUX_AMD64_DYNAMIC)\
	 build/$(LINUX_AMD64_STATIC)
 endif
	$(RM) -r\
	 build/windows-i686\
	 build/windows-i686-static\
	 build/windows-amd64\
	 build/windows-amd64-static
	$(RM)\
	 build/$(WINDOWS_I686_DYNAMIC)\
	 build/$(WINDOWS_I686_STATIC)\
	 build/$(WINDOWS_AMD64_DYNAMIC)\
	 build/$(WINDOWS_AMD64_STATIC)

.PHONY: clean-release
clean-release: clean-built-release | lodepng src
 ifneq ($(OS),Windows_NT)
	$(MAKE) -w -C lodepng builddir=build/linux-i686 clean
	$(MAKE) -w -C lodepng builddir=build/linux-amd64 clean
	$(MAKE) -w -C src STATIC_BUILD=0 builddir=build/linux-i686 clean
	$(MAKE) -w -C src STATIC_BUILD=1 builddir=build/linux-i686-static clean
	$(MAKE) -w -C src STATIC_BUILD=0 builddir=build/linux-amd64 clean
	$(MAKE) -w -C src STATIC_BUILD=1 builddir=build/linux-amd64-static clean
 endif
	$(MAKE) -w -C lodepng BUILD=mingw32 builddir=build/windows-i686 clean
	$(MAKE) -w -C lodepng BUILD=mingw64 builddir=build/windows-amd64 clean
	$(MAKE) -w -C src BUILD=mingw32 STATIC_BUILD=0 builddir=build/windows-i686 clean
	$(MAKE) -w -C src BUILD=mingw32 STATIC_BUILD=1 builddir=build/windows-i686-static clean
	$(MAKE) -w -C src BUILD=mingw64 STATIC_BUILD=0 builddir=build/windows-amd64 clean
	$(MAKE) -w -C src BUILD=mingw64 STATIC_BUILD=1 builddir=build/windows-amd64-static clean

.PHONY: distclean-release
distclean-release: clean-built-release | lodepng src
 ifneq ($(OS),Windows_NT)
	$(MAKE) -w -C lodepng builddir=build/linux-i686 distclean
	$(MAKE) -w -C lodepng builddir=build/linux-amd64 distclean
	$(MAKE) -w -C src STATIC_BUILD=0 builddir=build/linux-i686 distclean
	$(MAKE) -w -C src STATIC_BUILD=1 builddir=build/linux-i686-static distclean
	$(MAKE) -w -C src STATIC_BUILD=0 builddir=build/linux-amd64 distclean
	$(MAKE) -w -C src STATIC_BUILD=1 builddir=build/linux-amd64-static distclean
 endif
	$(MAKE) -w -C lodepng BUILD=mingw32 builddir=build/windows-i686 distclean
	$(MAKE) -w -C lodepng BUILD=mingw64 builddir=build/windows-amd64 distclean
	$(MAKE) -w -C src BUILD=mingw32 STATIC_BUILD=0 builddir=build/windows-i686 distclean
	$(MAKE) -w -C src BUILD=mingw32 STATIC_BUILD=1 builddir=build/windows-i686-static distclean
	$(MAKE) -w -C src BUILD=mingw64 STATIC_BUILD=0 builddir=build/windows-amd64 distclean
	$(MAKE) -w -C src BUILD=mingw64 STATIC_BUILD=1 builddir=build/windows-amd64-static distclean

#-----------------------------------------------------------------------------
# Common: install, install-strip, uninstall, clean, distclean

.PHONY: install
install: install-lodepng install-tools

.PHONY: install-strip
install-strip:
	$(MAKE) -w INSTALL_PROGRAM='$(INSTALL_PROGRAM) -s' install

.PHONY: uninstall
uninstall: uninstall-lodepng uninstall-tools

.PHONY: clean
clean: clean-lodepng clean-tools

.PHONY: distclean
distclean: distclean-lodepng distclean-tools
	$(RM) -r build
