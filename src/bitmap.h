/*
 * bitmap.h - header file for "bitmap.c".
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _BITMAP_H_INCLUDED
#define _BITMAP_H_INCLUDED

/* Default values */
#define DEF_PAPER_COL 7
#define DEF_INK_COL 0

/* ZX Spectrum screen address conversion */
#define SCR_ADDR(x, y, ls) ((((y)%8)*8 + (((y)/8)%8) + ((y)&~63))*(ls) + (x))

/* RCS screen address conversion */
#define RCS_ADDR(x, y) ((x)*64 + ((y)%64) + ((y)/64)*2048)

#define BM_BITMAP_SIZE(w, h) ((((w)+7)>>3)*(h))
#define BM_ATTRS_SIZE(w, h) ((((w)+7)>>3)*(((h)+7)>>3))
#define BM_FULL_SIZE(w, h) ((((w)+7)>>3)*((h)+(((h)+7)>>3)))

/* Flags of bitmap */
typedef unsigned bitmap_flags_t;
#define BMF_ADR_MASK   3
#define BMF_ADR_LINEAR 0      /* linear address mode */
#define BMF_ADR_SCREEN 1      /* ZX Spectrum's screen address mode */
#define BMF_ADR_RCS    2      /* ZX Spectrum's RCS address mode */
#define BMF_ATTRS      (1<<2) /* color attributes are available */

struct bitmap_t {
  bitmap_flags_t flags;
  unsigned char *data;  /* image data */
  size_t size;          /* image data size */
  size_t bitmap_size;   /* bitmap size */
  size_t attrs_size;    /* color attributes size */
  unsigned w;           /* image width */
  unsigned h;           /* image height */
  char *filename;
};

/* Flags to setup bitmap parameters */
typedef unsigned bitmap_setup_flags_t;
#define BMSF_ADR (1<<0)   /* address mode is set */
#define BMSF_ATTRS (1<<1) /* attributes flag is set */

/* Get an error message of a last failed bitmap-routine */
const char *bitmap_error_text ();

const char *get_bitmap_type_str (bitmap_setup_flags_t sf,
  bitmap_flags_t flags);

int check_bitmap_std_size (size_t size);
int check_bitmap_std_dimensions (unsigned w, unsigned h);
int get_bitmap_std_params (bitmap_flags_t *flags, unsigned *w, unsigned *h,
  size_t size);
int check_bitmap_params (size_t size, unsigned w, unsigned h);
int get_bitmap_params (bitmap_flags_t *flags, size_t size, unsigned w,
  unsigned h);
int resolve_bitmap_params (bitmap_flags_t *flags, unsigned *w, unsigned *h,
  bitmap_setup_flags_t sf, bitmap_flags_t in_flags, size_t in_size,
  unsigned in_w, unsigned in_h);

struct bitmap_t *bitmap_new ();
struct bitmap_t *bitmap_dup (struct bitmap_t *self);
void bitmap_free (struct bitmap_t *self);
void bitmap_dispose (struct bitmap_t **self);
void bitmap_assign (struct bitmap_t *self, bitmap_flags_t flags,
  unsigned char *data, unsigned w, unsigned h);
int bitmap_set_filename (struct bitmap_t *self, const char *filename);
int bitmap_save_as (struct bitmap_t *self, const char *filename);
int bitmap_save_attributes_as (struct bitmap_t *self, const char *filename);

int import_bitmap_from_memory (struct bitmap_t **self,
  bitmap_setup_flags_t sf, bitmap_flags_t flags, unsigned char *data,
  size_t size, unsigned w, unsigned h);
int import_bitmap_from_file (struct bitmap_t **self, const char *filename,
  bitmap_setup_flags_t sf, bitmap_flags_t flags, unsigned w, unsigned h);
int import_bitmap_from_image (struct bitmap_t **self,
  bitmap_setup_flags_t sf, bitmap_flags_t flags, unsigned w, unsigned h,
  struct image_t *image, unsigned char *attrs);

struct bitmap_t *export_bitmap_as_bitmap (struct bitmap_t *self,
  bitmap_flags_t flags, unsigned w, unsigned h, unsigned char *attrs);
struct image_t *export_bitmap_as_image (struct bitmap_t *self,
  enum image_color_type_t colortype, unsigned bitdepth, unsigned char *attrs);

#endif  /* !_BITMAP_H_INCLUDED */
