/*
 * font.c - routines to deal with a binary font.
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "font.h"

/* An error message of a last failed font-routine */
static const char *font_err = NULL;

static void font_error (const char *text) {
  font_err = text;
}

const char *font_error_text () {
  return font_err;
}

const char *get_font_type_str (/*nothing for now*/) {
  return "binary font";
}

struct font_t *font_new () {
  struct font_t *self = calloc (1, sizeof (struct font_t));
  if (!self) font_error ("Memory allocation failed");
  return self;
}

struct font_t *font_dup (struct font_t *self) {
  struct font_t *font = font_new ();
  if (!font) goto error_exit;

  /*font->data = NULL;*/ /* not needed */
  if (self->data && self->size) {
    font->data = malloc (self->size);
    if (!font->data) {
      font_error ("Memory allocation failed");
      goto error_exit;
    }
    memcpy (font->data, self->data, self->size);
  }
  font->size = self->size;
  font->cw = self->cw;
  font->ch = self->ch;
  /*font->filename = NULL;*/ /* not needed */
  if (self->filename)
    if (str_dup (&font->filename, self->filename)) {
      font_error ("Memory allocation failed");
      goto error_exit;
    }

  return font;

error_exit:
  if (font) font_dispose (&font);
  return NULL;
}

void font_free (struct font_t *self) {
  if (self->filename) {
    free (self->filename);
    self->filename = NULL;
  }
  if (self->data) {
    free (self->data);
    self->data = NULL;
  }
}

void font_dispose (struct font_t **self) {
  if (*self) {
    font_free (*self);
    free (*self);
    *self = NULL;
  }
}

void font_assign (struct font_t *self, unsigned char *data, size_t size,
  unsigned cw, unsigned ch) {

  self->data = data;
  self->size = size;
  self->cw = cw;
  self->ch = ch;
}

/* Returns 0 on success, -1 on error */
int font_set_filename (struct font_t *self, const char *filename) {
  int status = str_dup (&self->filename, filename);
  if (status) font_error ("Memory allocation failed");
  return status;
}

/* Returns 0 on success, -1 on error, 1 if wrong data/character size */
int resolve_font_params (size_t size, unsigned cw, unsigned ch) {
  if (!(size && cw && ch)) {
    font_error ("Invalid parameters");
    return -1;
  }
  if (size % FNT_CHAR_SIZE (cw, ch)) {
    font_error ("Wrong data size or character size");
    return 1; /* not a font */
  }
  return 0;
}

/* Returns 0 on success, -1 on error */
int font_save_as (struct font_t *self, const char *filename) {
  int status = file_save (self->data, self->size, filename);
  if (status) font_error ("Failed to write output file");
  return status;
}

/* Returns 0 on success, -1 on error */
static int font_convert_to_image_GREY8 (struct font_t *self, struct image_t *image,
  unsigned cwo, unsigned cho, unsigned wco, unsigned def_wco, bool rotate) {

  unsigned ls, cs, c, w, hc, s, cw, ch, n, y;
  unsigned dmax, kmax, dj0, dj;
  unsigned char *p, *src;

  if (!(self->data && self->size)) {
    font_error ("No data in input font");
    return -1;
  }

  if (!(self->cw && self->ch)) {
    font_error ("Wrong character size for input font");
    return -1;
  }

  if (!(cwo && cho)) {
    font_error ("Wrong character size for output image");
    return -1;
  }

  if (!rotate) {
    /* input character line size (horizontal) in bytes */
    ls = FNT_CHAR_SIZE (self->cw, 1);
    /* input character size in bytes */
    cs = ls * self->ch;
  } else {
    /* width and height are swapped */
    ls = FNT_CHAR_SIZE (self->ch, 1);
    cs = ls * self->cw;
  }
  /* input characters count */
  c = self->size / cs;
  if (self->size % cs) {
    font_error ("Wrong data size or character size in input font");
    return -1;
  }

  /* determine count of characters per line for output if needed */
  if (!wco) wco = (c < def_wco) ? c : def_wco;

  /* output image width in pixels */
  w = cwo * wco;
  /* output image height in characters (rounded up to "wco" pcs.) */
  hc = (c + wco - 1) / wco;
  /* output image size in bytes */
  s = w * cho * hc;
  p = calloc (1, s);
  if (!p) {
    error_malloc (s);
    return -1;
  }

  /* input character width to convert into output */
  cw = self->cw;
  if (cw > cwo) cw = cwo;
  /* input character height to convert into output */
  ch = self->ch;
  if (ch > cho) ch = cho;

  src = self->data;

  if (!rotate) {
    dmax = ch;
    kmax = cw;
    dj0 = w;
    dj = 1;
  } else {
    /* width and height are swapped */
    dmax = cw;
    kmax = ch;
    dj0 = 1;
    dj = w;
  }

  /* current character index */
  n = 0;
  for (y = 0; y < hc; y++) {
    unsigned x;

    for (x = 0; x < wco; x++) {
      unsigned i0 = cs * n;
      unsigned j0 = w * cho * y + cwo * x;
      unsigned d;

      for (d = 0; d < dmax; d++) {
        unsigned i = i0;
        unsigned j = j0;
        unsigned k = kmax;

        /* Convert whole bytes */
        while (k >= 8) {
          unsigned char a = src[i++];
          unsigned char l = 8;
          do {
            p[j] = (a & 128) ? 255 : 0;
            j += dj;
            a <<= 1;
            l--;
          } while (a && l);
          if (l)  /* skip the rest if any */
            j += dj * l;
          k -= 8;
        }

        /* Partially convert last byte */
        if (k) {
          unsigned char a = src[i++];
          do {
            p[j] = (a & 128) ? 255 : 0;
            j += dj;
            a <<= 1;
            k--;
          } while (a && k);
        }

        i0 += ls;
        j0 += dj0;
      }

      n++;
      if (n == c) break;
    }
    if (n == c) break;
  }

  image_assign_raw (image, ICT_GREY, 8, p, s, w, cho * hc);
  return 0;
}

/* Returns 0 on success, -1 on error */
int font_convert_from_image_GREY8 (struct font_t *self, unsigned cwo, unsigned cho,
  struct image_t *image, unsigned cwi, unsigned chi, bool rotate) {

  unsigned wc, hc, c, ls, cs, s, cw, ch, n, y;
  unsigned dmax, kmax, dj0, dj;
  unsigned char *p, *src;

  if (image->type != IMGT_RAW) {
    font_error ("Wrong type of input image");
    return -1;
  }

  if ((image->colortype != ICT_GREY)
  ||  (image->bitdepth != 8)) {
    font_error ("Wrong internal format of input image");
    return -1;
  }

  if (!(image->data && image->size)) {
    font_error ("No data in input image");
    return -1;
  }

  if (!(image->w && image->h && cwi && chi
  &&  ((image->w % cwi) == 0)
  &&  ((image->h % chi) == 0))) {
    font_error ("Wrong size or character size of input image");
    return -1;
  }

  if (!(cwo && cho)) {
    font_error ("Wrong character size for output font");
    return -1;
  }

  /* input image width in characters */
  wc = image->w / cwi;
  /* input image height in characters */
  hc = image->h / chi;
  /* input characters count */
  c = wc * hc;

  if (!rotate) {
    /* output character line size (horizontal) in bytes */
    ls = FNT_CHAR_SIZE (cwo, 1);
    /* output character size in bytes */
    cs = ls * cho;
  } else {
    /* width and height are swapped */
    ls = FNT_CHAR_SIZE (cho, 1);
    cs = ls * cwo;
  }
  /* output font size in bytes */
  s = cs * c;
  p = calloc (1, s);
  if (!p) {
    font_error ("Memory allocation failed");
    return -1;
  }

  /* input character width to convert into output */
  cw = cwi;
  if (cw > cwo) cw = cwo;
  /* input character height to convert into output */
  ch = chi;
  if (ch > cho) ch = cho;

  src = image->data;

  if (!rotate) {
    dmax = ch;
    kmax = cw;
    dj0 = image->w;
    dj = 1;
  } else {
    /* width and height are swapped */
    dmax = cw;
    kmax = ch;
    dj0 = 1;
    dj = image->w;
  }

  /* current character offset */
  n = 0;
  for (y = 0; y < hc; y++) {
    unsigned x;

    for (x = 0; x < wc; x++) {
      unsigned i0 = n;
      unsigned j0 = image->w * chi * y + cwi * x;
      unsigned d;

      for (d = 0; d < dmax; d++) {
        unsigned i = i0, j = j0, k = kmax;

        /* Convert whole bytes */
        while (k >= 8) {
          unsigned char a = 0, l = 8;
          do {
            a = (a << 1) | (src[j] ? 1 : 0);
            j += dj;
          } while (--l);
          p[i++] = a;
          k -= 8;
        }

        /* Partially convert last byte */
        if (k) {
          unsigned char a = 0, l = 8 - k;
          do {
            a = (a << 1) | (src[j] ? 1 : 0);
            j += dj;
          } while (--k);
          p[i++] = a << l;
        }

        i0 += ls;
        j0 += dj0;
      }

      n += cs;
    }
  }

  font_assign (self, p, s, cwo, cho);
  return 0;
}

/* Returns 0 on success, -1 on error, 1 if not a font */
int import_font_from_memory (struct font_t **self, unsigned char *data,
  size_t size, unsigned cw, unsigned ch) {

  int status; /* exit status */
  struct font_t *font = NULL;
  unsigned char *font_data = NULL;

  status = resolve_font_params (size, cw, ch);
  if (status) goto error_exit;

  font = font_new ();
  if (!font) { status = -1; goto error_exit; }

  font_data = malloc (size);
  if (!font_data) {
    font_error ("Memory allocation failed");
    status = -1; goto error_exit;
  }
  memcpy (font_data, data, size);
  font_assign (font, font_data, size, cw, ch);
  *self = font;
  return 0;

error_exit:
  if (font) font_dispose (&font);
  if (font_data) free (font_data);
  return status;
}

/* Returns 0 on success, -1 on error, 1 if not a font */
int import_font_from_file (struct font_t **self, const char *filename,
  unsigned cw, unsigned ch) {

  int status; /* exit status */
  unsigned char *data = NULL;
  size_t size;
  struct font_t *font = NULL;

  status = file_load (&data, &size, filename);
  if (status) {
    font_error ("Failed to load input file");
    goto error_exit;
  }

  status = import_font_from_memory (&font, data, size, cw, ch);
  if (status) goto error_exit;

  status = font_set_filename (font, filename);
  if (status) goto error_exit;

  free (data);
  *self = font;
  return 0;

error_exit:
  if (data) free (data);
  if (font) font_dispose (&font);
  return status;
}

/* Returns 0 on success, -1 on error, 1 if not a font */
int import_font_from_image (struct font_t **self, unsigned cwo, unsigned cho,
  struct image_t *image, unsigned cwi, unsigned chi, bool rotate) {

  int status; /* exit status*/
  struct font_t *font = NULL;

  font = font_new ();
  if (!font) { status = -1;  goto error_exit; }

  if ((image->type == IMGT_RAW)
  &&  (image->colortype == ICT_GREY)
  &&  (image->bitdepth == 8)) {
    status = font_convert_from_image_GREY8 (
      font, cwo, cho, image, cwi, chi, rotate);
  } else {
    font_error ("Wrong internal format of output image");
    status = -1;
  }
  if (status) goto error_exit;

  *self = font;
  return 0;

error_exit:
  if (font) font_dispose (&font);
  return status;
}

struct image_t *export_font_as_image (struct font_t *self,
  enum image_color_type_t colortype, unsigned bitdepth,
  unsigned cwo, unsigned cho, unsigned wco, unsigned def_wco, bool rotate) {

  struct image_t *image = NULL;

  image = image_new ();
  if (!image) {
    font_error ("Memory allocation failed");
    goto error_exit;
  }

  if ((colortype == ICT_GREY)
  &&  (bitdepth == 8)) {
    if (font_convert_to_image_GREY8 (
      self, image, cwo, cho, wco, def_wco, rotate))
      goto error_exit;
  } else {
    font_error ("Wrong internal format of output image");
    goto error_exit;
  }

  return image;

error_exit:
  if (image) image_dispose (&image);
  return NULL;
}
