/*
 * bitmap.c - routines to deal with a bitmap.
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <stdarg.h>
#include <stdio.h>
#include "image.h"
#include "bitmap.h"

/* Flags for error message of the last bitmap-routine */
#define BMEF_MEM_MASK   (1<<0)
#define BMEF_MEM_CONST  (0<<0)
#define BMEF_MEM_VAR    (1<<0)

/* An error message of a last failed bitmap-routine */
static const char *bitmap_err = NULL;
static unsigned bitmap_err_flags = BMEF_MEM_CONST;

/* Bitmap types and parameters */
static const struct {
  /* key: sf, flags (unique) */
  const char *type_str;
  bitmap_setup_flags_t sf;
  bitmap_flags_t flags;
} bitmap_types[] = {
  { "bitmap",
    0, 0 },
  { "bitmap (w/o attributes)",
    BMSF_ATTRS, 0 },
  { "bitmap (w/attributes)",
    BMSF_ATTRS, BMF_ATTRS },

  { "linear bitmap",
    BMSF_ADR, BMF_ADR_LINEAR },
  { "linear bitmap (w/o attributes)",
    BMSF_ADR | BMSF_ATTRS, BMF_ADR_LINEAR },
  { "linear bitmap (w/attributes)",
    BMSF_ADR | BMSF_ATTRS, BMF_ADR_LINEAR | BMF_ATTRS},

  { "screen",
    BMSF_ADR, BMF_ADR_SCREEN },
  { "screen (w/o attributes)",
    BMSF_ADR | BMSF_ATTRS, BMF_ADR_SCREEN },
  { "screen (w/attributes)",
    BMSF_ADR | BMSF_ATTRS, BMF_ADR_SCREEN | BMF_ATTRS },

  { "RCS",
    BMSF_ADR, BMF_ADR_RCS },
  { "RCS (w/o attributes)",
    BMSF_ADR | BMSF_ATTRS, BMF_ADR_RCS },
  { "RCS (w/attributes)",
    BMSF_ADR | BMSF_ATTRS, BMF_ADR_RCS | BMF_ATTRS },

  { NULL, 0, 0 }
};

static const struct {
  /* key: size (unique) */
  size_t size;
  unsigned w, h;
  bitmap_flags_t flags;
} std_bitmaps[] = {
  { BM_BITMAP_SIZE (256, 64),  256, 64,  BMF_ADR_SCREEN },
  { BM_FULL_SIZE   (256, 64),  256, 64,  BMF_ADR_SCREEN | BMF_ATTRS },
  { BM_BITMAP_SIZE (256, 128), 256, 128, BMF_ADR_SCREEN },
  { BM_FULL_SIZE   (256, 128), 256, 128, BMF_ADR_SCREEN | BMF_ATTRS },
  { BM_BITMAP_SIZE (256, 192), 256, 192, BMF_ADR_SCREEN },
  { BM_FULL_SIZE   (256, 192), 256, 192, BMF_ADR_SCREEN | BMF_ATTRS },
  { 0 }
};

static void bitmap_err_assign (const char *text, unsigned flags) {
  bitmap_err = text;
  bitmap_err_flags = flags;
}

/* Free previous error message */
static void bitmap_err_free () {
  if (bitmap_err
  &&  ((bitmap_err_flags & BMEF_MEM_MASK) == BMEF_MEM_VAR))
    free ((void *) bitmap_err);
}

static void bitmap_error (const char *text) {
  bitmap_err_free ();
  bitmap_err_assign (text, BMEF_MEM_CONST);
}

/* Formatted version of bitmap_error() */
static void bitmap_error_fmt (const char *fmt, ...) {
  va_list ap;
  int n;
  size_t size = 0;
  char *s = NULL;

  bitmap_err_free ();

  /* Determine required size */
  va_start (ap, fmt);
  n = vsnprintf (s, size, fmt, ap);
  va_end (ap);

  if (n < 0) {
    bitmap_err_assign ("Internal failure in vsnprintf()", BMEF_MEM_CONST);
    return;
  }

  /* One extra byte for '\0' */
  size = (size_t) n + 1;
  s = malloc (size);
  if (!s) {
    bitmap_err_assign ("Memory allocation failed", BMEF_MEM_CONST);
    return;
  }

  va_start (ap, fmt);
  n = vsnprintf (s, size, fmt, ap);
  va_end (ap);

  if (n < 0) {
    free (s);
    bitmap_err_assign ("Internal failure in vsnprintf()", BMEF_MEM_CONST);
    return;
  }

  bitmap_err_assign (s, BMEF_MEM_VAR);
}

const char *bitmap_error_text () {
  return bitmap_err;
}

const char *get_bitmap_type_str (bitmap_setup_flags_t sf,
  bitmap_flags_t flags) {

  bitmap_flags_t mask = 0;
  int i;

  /* use only known flags */
  if (sf & BMSF_ADR)   mask |= BMF_ADR_MASK;
  if (sf & BMSF_ATTRS) mask |= BMF_ATTRS;
  flags &= mask;

  for (i = 0; bitmap_types[i].type_str; i++)
    if ((bitmap_types[i].sf == sf)
    &&  (bitmap_types[i].flags == flags))
      return bitmap_types[i].type_str;

  bitmap_error ("Bitmap has unknown internal format");
  return "unknown type of bitmap";
}

/* Returns 0 if standard size, -1 if custom */
int check_bitmap_std_size (size_t size) {
  int i;

  for (i = 0; std_bitmaps[i].size; i++)
    if (std_bitmaps[i].size == size)
      return 0;

  bitmap_error ("Bitmap has non-standard data size");
  return -1;
}

/* Returns 0 if standard dimensions, -1 if custom */
int check_bitmap_std_dimensions (unsigned w, unsigned h) {
  int i;

  for (i = 0; std_bitmaps[i].size; i++)
    if ((std_bitmaps[i].w == w) && (std_bitmaps[i].h == h))
      return 0;

  bitmap_error ("Bitmap has non-standard dimensions");
  return -1;
}

/* Returns 0 on success, 1 if not standard image size */
int get_bitmap_std_params (bitmap_flags_t *flags, unsigned *w, unsigned *h,
  size_t size) {

  int i;

  for (i = 0; std_bitmaps[i].size; i++)
    if (std_bitmaps[i].size == size) {
      *flags = std_bitmaps[i].flags;
      *w = std_bitmaps[i].w;
      *h = std_bitmaps[i].h;
      return 0;
    }

  bitmap_error ("Bitmap has non-standard data size");
  return -1;
}

/* Returns 0 on success, -1 on error */
int check_bitmap_params (size_t size, unsigned w, unsigned h) {
  return
    ((size == BM_BITMAP_SIZE (w, h))
  || (size == BM_FULL_SIZE (w, h))) ? 0 : -1;
}

int get_bitmap_params (bitmap_flags_t *flags, size_t size, unsigned w,
  unsigned h) {

  if (size == BM_BITMAP_SIZE (w, h)) {
    *flags = BMF_ADR_LINEAR;
    return 0;
  } else if (size == BM_FULL_SIZE (w, h)) {
    *flags = BMF_ADR_LINEAR | BMF_ATTRS;
    return 0;
  } else {
    bitmap_error ("Bitmap has wrong data size or dimensions");
    return -1;
  }
}

/* Returns 0 on success, -1 on error, 1 if wrong data/image size */
int resolve_bitmap_params (bitmap_flags_t *flags, unsigned *w, unsigned *h,
  bitmap_setup_flags_t sf, bitmap_flags_t in_flags, size_t in_size,
  unsigned in_w, unsigned in_h) {

  int status;
  bitmap_flags_t _flags;
  unsigned _w, _h;

  if (!in_size) {
    bitmap_error ("Invalid parameters");
    return -1;  /* invalid parameters */
  }

  /* find bitmap by it's size in memory */
  status = get_bitmap_std_params (&_flags, &_w, &_h, in_size);

  /* resolve using specified flags */
  if (in_w && in_h) {
    bool a; /* attributes found */
    if (in_size == BM_BITMAP_SIZE (in_w, in_h))
      a = false;
    else if (in_size == BM_FULL_SIZE (in_w, in_h))
      a = true;
    else {
      bitmap_error ("Bitmap has wrong data size");
      return 1; /* not a bitmap (wrong size) */
    }

    /* check for standard params */
    if (status) {
      /* non-standard params */
      if (a)
        _flags = BMF_ADR_LINEAR | BMF_ATTRS;
      else
        _flags = BMF_ADR_LINEAR;

      /* check if specified "address mode" differs */
      if ((sf & BMSF_ADR)
      &&  ((in_flags & BMF_ADR_MASK) != BMF_ADR_LINEAR)) {
        bitmap_error ("Bitmap has address mode mismatch");
        return 1; /* not a bitmap ("address mode" mismatch) */
      }
    } else {
      /* standard params */
      /* no limits for "address mode": safe to overwrite it */
      if (sf & BMSF_ADR)
        _flags = (_flags & ~BMF_ADR_MASK) | (in_flags & BMF_ADR_MASK);
    }

    /* check "attributes": must be the same if set */
    if (sf & BMSF_ATTRS)
      if ((_flags & BMF_ATTRS) != (in_flags & BMF_ATTRS)) {
        bitmap_error ("Bitmap has attributes data mismatch");
        return 1; /* not a bitmap ("attributes" mismatch) */
      }

    *flags = _flags;
    /* save user-specified values */
    *w = in_w;
    *h = in_h;
    return 0;

  } else {
    if (!status) {
      /* standard params */
      /* no limits for "address mode": safe to overwrite it */
      if (sf & BMSF_ADR)
        _flags = (_flags & ~BMF_ADR_MASK) | (in_flags & BMF_ADR_MASK);

      if (sf & BMSF_ATTRS)
        if ((_flags & BMF_ATTRS) != (in_flags & BMF_ATTRS)) {
          bitmap_error ("Bitmap has attributes data mismatch");
          return 1; /* not a bitmap ("attributes" mismatch) */
        }

      *flags = _flags;
      /* save found values */
      *w = _w;
      *h = _h;
    }
    return status;
  }
}

struct bitmap_t *bitmap_new () {
  struct bitmap_t *self = calloc (1, sizeof (struct bitmap_t));
  if (!self) bitmap_error ("Memory allocation failed");
  return self;
}

struct bitmap_t *bitmap_dup (struct bitmap_t *self) {
  struct bitmap_t *bitmap = bitmap_new ();
  if (!bitmap) goto error_exit;

  bitmap->flags = self->flags;
  /*bitmap->data = NULL;*/ /* not needed */
  if (self->data && self->size) {
    bitmap->data = malloc (self->size);
    if (!bitmap->data) {
      bitmap_error ("Memory allocation failed");
      goto error_exit;
    }
    memcpy (bitmap->data, self->data, self->size);
  }
  bitmap->size = self->size;
  bitmap->bitmap_size = self->bitmap_size;
  bitmap->attrs_size = self->attrs_size;
  bitmap->w = self->w;
  bitmap->h = self->h;
  /*bitmap->filename = NULL;*/ /* not needed */
  if (self->filename)
    if (str_dup (&bitmap->filename, self->filename)) {
      bitmap_error ("Memory allocation failed");
      goto error_exit;
    }

  return bitmap;

error_exit:
  if (bitmap) bitmap_dispose (&bitmap);
  return NULL;
}

void bitmap_free (struct bitmap_t *self) {
  if (self->filename) {
    free (self->filename);
    self->filename = NULL;
  }
  if (self->data) {
    free (self->data);
    self->data = NULL;
  }
}

void bitmap_dispose (struct bitmap_t **self) {
  if (*self) {
    bitmap_free (*self);
    free (*self);
    *self = NULL;
  }
}

void bitmap_assign (struct bitmap_t *self, bitmap_flags_t flags,
  unsigned char *data, unsigned w, unsigned h) {

  self->flags = flags;
  self->data = data;
  self->bitmap_size = BM_BITMAP_SIZE (w, h);
  self->attrs_size = (flags & BMF_ATTRS) ? BM_ATTRS_SIZE (w, h) : 0;
  self->size = self->bitmap_size + self->attrs_size;
  self->w = w;
  self->h = h;
}

/* Returns 0 on success, -1 on error */
int bitmap_set_filename (struct bitmap_t *self, const char *filename) {
  int status = str_dup (&self->filename, filename);
  if (status) bitmap_error ("Memory allocation failed");
  return status;
}

static int make_zx_attribute (unsigned char *ncols, unsigned char *attr,
  const unsigned char *pixels, unsigned pixels_count) {

  unsigned char buf[16];
  unsigned i, count[2];
  /* to remove "maybe-uninitialized" warning (GCC): */
  unsigned char ink = 0, paper = 0, a;

  memset (buf, 0, sizeof (buf));
  for (i = 0; i < pixels_count; i++) {
    unsigned char c = pixels[i];
    if (c > 15) {
      bitmap_error ("Bad color index in input data");
      return -1;
    }
    if (i == 0) ink = c;
    if (i == 1) paper = c;
    /* try to find first unique color */
    if ((i > 1) && (ink == paper) && (paper != c)) paper = c;
    buf[c]++;
  }

  /* group of colors with brightness set to 0 */
  count[0] = 0; for (i = 0; i < 8; i++) if (buf[i]) count[0]++;
  /* group of colors with brightness set to 1 */
  count[1] = 0; for (i = 8; i < 16; i++) if (buf[i]) count[1]++;

  if (buf[0] && count[1]) {
    /* move black color into a "bright" group */
    count[0]--;
    count[1]++;
  }
  if (count[0] + count[1] > 2) {
    bitmap_error ("There are more than 2 different colors in one group");
    return -1;
  }
  if (count[0] && count[1]) {
    bitmap_error ("Pixels have two different brightness levels in one group");
    return -1;
  }

  if (count[0] + count[1] == 2) {
    *ncols = 2;
    /* swap colors if needed */
    if ((buf[ink] > buf[paper])
    || ((buf[ink] == buf[paper]) && (ink < paper))) {
      unsigned char t; t = ink; ink = paper; paper = t;
    }
  } else
    *ncols = 1;

  a = (ink & 7) + ((paper & 7) << 3);
  if (count[1]) a |= 0x40;
  *attr = a;
  return 0;
}

/*
  From:   (0 0 0 S1 S0 C4 C3 C2 | C1 C0 R2 R1 R0 L2 L1 L0)
  To:     (0 1 0 S1 S0 L2 L1 L0 | R2 R1 R0 C4 C3 C2 C1 C0)
  Screen: (0 1 0 Y7 Y6 Y2 Y1 Y0 | Y5 Y4 Y3 X4 X3 X2 X1 X0)
  X:      (0 0 0 C4 C3 C2 C1 C0)
  Y:      (S1 S0 R2 R1 R0 L2 L1 L0)
*/
/* Returns 0 on success, -1 on error */
static int bitmap_convert_from_bitmap (struct bitmap_t *self,
  bitmap_flags_t flags, unsigned w, unsigned h, struct bitmap_t *bitmap,
  unsigned char *attrs) {

  unsigned bc, ls0, ls1, lc, ha, y0, ya;
  size_t size;
  unsigned char *data, *p;

  if (flags & BMF_ATTRS)
    size = BM_FULL_SIZE (w, h);
  else
    size = BM_BITMAP_SIZE (w, h);

  data = calloc (1, size);
  if (!data) {
    bitmap_error ("Memory allocation failed");
    goto error_exit;
  }

  /* input bitmap address */
  p = bitmap->data;
  /* line size in bytes of input bitmap */
  ls0 = BM_BITMAP_SIZE (bitmap->w, 1);
  /* line size in bytes of output bitmap */
  ls1 = BM_BITMAP_SIZE (w, 1);
  /* bytes count per line to copy from input bitmap */
  bc = (ls1 > ls0) ? ls0 : ls1;
  /* lines count to copy from input bitmap */
  lc = (h > bitmap->h) ? bitmap->h : h;
  /* height of output bitmap in attributes */
  ha = (lc + 7) >> 3;

  /* copy data by blocks of 8x8 pixels or less (if on edge of bitmap) */
  y0 = 0;
  for (ya = 0; ya < ha; ya++) {
    unsigned o0, o1, ao0, ao1, x0, xa;
    unsigned char *ba0, *ba1;
    /* from */
    switch (bitmap->flags & BMF_ADR_MASK) {
      case BMF_ADR_LINEAR:
        o0 = y0 * ls0;
        break;
      case BMF_ADR_SCREEN:
        o0 = SCR_ADDR (0, y0, ls0);
        break;
      case BMF_ADR_RCS:
        o0 = 0; /* must be set in inner loop for each X position */
        break;
      default:
        o0 = 0; /* needed to remove "maybe-uninitialized" warning (GCC) */
        break;
    }
    /* to */
    switch (flags & BMF_ADR_MASK) {
      case BMF_ADR_LINEAR:
        o1 = y0 * ls1;
        break;
      case BMF_ADR_SCREEN:
        o1 = SCR_ADDR (0, y0, ls0);
        break;
      case BMF_ADR_RCS:
        o1 = 0; /* must be set in inner loop for each X position */
        break;
      default:
        o1 = 0; /* needed to remove "maybe-uninitialized" warning (GCC) */
        break;
    }
    if (bitmap->flags & BMF_ATTRS) {
      ba0 = bitmap->data + bitmap->bitmap_size;
      ao0 = ls0 * ya;
    } else {
      /* needed to remove "maybe-uninitialized" warning (GCC): */
      ba0 = NULL;
      ao0 = 0;
    }
    if (flags & BMF_ATTRS) {
      ba1 = data + BM_BITMAP_SIZE (w, h);
      ao1 = ls1 * ya;
    } else {
      /* needed to remove "maybe-uninitialized" warning (GCC): */
      ba1 = NULL;
      ao1 = 0;
    }
    x0 = 0;
    for (xa = 0; xa < bc; xa++) {
      unsigned char bit[8];
      unsigned cw, ch, o, cy, y;
      unsigned char attr0, attr1;

      /* cell's width */
      cw = (w < bitmap->w) ? w : bitmap->w;
      cw -= x0;
      if (cw > 8) cw = 8;
      /* cell's height */
      ch = lc - y0;
      if (ch > 8) ch = 8;

      /* read bitmap data */
      o = o0;
      for (cy = 0, y = y0; cy < ch; cy++, y++) {
        /* RCS is a special case */
        if ((bitmap->flags & BMF_ADR_MASK) == BMF_ADR_RCS)
          o = RCS_ADDR (xa, y);
        bit[cy] = p[o];
        if ((bitmap->flags & BMF_ADR_MASK) != BMF_ADR_RCS)
          o += ls0;
      }

      /* read attribute if any */
      attr0 = ba0 ? ba0[ao0++] : (DEF_INK_COL + (DEF_PAPER_COL << 3));
      attr1 = attrs ? attrs[ls1 * ya + xa] : attr0;

      /* convert pixel data if needed */
      if (ba0 && attrs && ((attr0 & 0x7f) != (attr1 & 0x7f))) {
        unsigned char pix[8*8], *buf;
        unsigned char ink0, paper0, ink, paper, ncols = 0;
        bool ok;
        /* check input and output attributes */
        if ((attr0 & (1<<6)) != (attr1 & (1<<6))) {
          bitmap_error_fmt (
            "Brightness does not match for cell (%u,%u)-(%u,%u)",
            x0, y0, x0 + cw - 1, y0 + ch - 1);
          goto error_exit;
        }
        /* convert bitmap into a block of pixels using an attribute */
        ink0 = attr0 & 7;
        paper0 = (attr0 >> 3) & 7;
        if (attr0 & (1<<6)) {
          /* adjust brightness */
          if (ink0) ink0 += 8;
          if (paper0) paper0 += 8;
        }
        buf = pix;
        for (cy = 0; cy < ch; cy++) {
          unsigned char a = bit[cy];
          unsigned char cx;
          /* convert a single byte into a group of pixels */
          for (cx = 0; cx < cw; cx++) {
            *buf = (a & (1<<7)) ? ink0 : paper0;
            a <<= 1;
            buf++;
          }
        }
        /* get real attribute */
        make_zx_attribute (&ncols, &attr0, pix, cw * ch);
        /* check target attribute */
        ink0 = attr0 & 7;
        ink = attr1 & 7;
        paper = (attr1 >> 3) & 7;
        if (ncols == 2) {
          paper0 = (attr0 >> 3) & 7;
          ok = ((ink0 == ink) && (paper0 == paper))
          ||   ((paper0 == ink) && (ink0 == paper));
        } else {
          ok = (ink0 == ink) || (ink0 == paper);
          if (ink == DEF_INK_COL)
            paper = DEF_PAPER_COL;
          else if (ink == DEF_PAPER_COL)
            paper = DEF_INK_COL;
          else
            paper = 0;
        }
        if (!ok) {
          bitmap_error_fmt (
            "Colors does not match for cell (%u,%u)-(%u,%u)",
            x0, y0, x0 + cw - 1, y0 + ch - 1);
          goto error_exit;
        }

        /* convert block of pixels into a bitmap using an attribute */
        buf = pix;
        for (cy = 0; cy < ch; cy++) {
          unsigned char b = 0;  /* clear bits */
          /* convert pixels into a single byte */
          if (ink != paper) {
            unsigned char m = 1<<7;
            unsigned char cx;
            for (cx = 0; cx < cw; cx++) {
              if (((*buf) & 7) == ink) b |= m;  /* set bit */
              m >>= 1;
              buf++;
            }
          }
          bit[cy] = b;
        }
      }

      /* write bitmap data */
      y = y0;
      o = o1;
      for (cy = 0; cy < ch; cy++) {
        /* RCS is a special case */
        if ((flags & BMF_ADR_MASK) == BMF_ADR_RCS)
          o = RCS_ADDR (xa, y);
        data[o] = bit[cy];
        if ((flags & BMF_ADR_MASK) != BMF_ADR_RCS)
          o += ls1;
        y++;
      }

      /* write attribute if needed */
      if (ba1)
        ba1[ao1++] = attr1;

      x0 += 8;  /* horizontal step */
    }

    y0 += 8;  /* vertical step */
  }

  bitmap_assign (self, flags, data, w, h);
  return 0;

error_exit:
  if (data) free (data);
  return -1;
}

/*
  From:   (0 0 0 S1 S0 C4 C3 C2 | C1 C0 R2 R1 R0 L2 L1 L0)
  To:     (0 1 0 S1 S0 L2 L1 L0 | R2 R1 R0 C4 C3 C2 C1 C0)
  Screen: (0 1 0 Y7 Y6 Y2 Y1 Y0 | Y5 Y4 Y3 X4 X3 X2 X1 X0)
  X:      (0 0 0 C4 C3 C2 C1 C0)
  Y:      (S1 S0 R2 R1 R0 L2 L1 L0)
*/
/* Returns 0 on success, -1 on error */
static int bitmap_convert_from_image_RGBA8 (struct bitmap_t *self,
  bitmap_setup_flags_t out_sf, bitmap_flags_t out_flags, unsigned out_w,
  unsigned out_h, struct image_t *image, unsigned char *attrs) {

  unsigned char *data = NULL;
  size_t size;
  bitmap_flags_t flags;
  unsigned w, h, wa, ha, ls0, ls1, y0, ya, ao;

  if (!((image->type == IMGT_RAW)
  &&  (image->colortype == ICT_RGBA)
  &&  (image->bitdepth == 8))) {
    bitmap_error ("Input image has wrong internal format");
    goto error_exit;
  }

  if (!(image->data && image->size)) {
    bitmap_error ("Input image is empty");
    goto error_exit;
  }

  if (!(image->w && ((image->w % 8) == 0)
  &&  image->h && ((image->h % 8) == 0))) {
    bitmap_error ("Input image has wrong dimensions");
    goto error_exit;
  }

  if (out_sf & BMSF_ATTRS) {
    if (out_flags & BMF_ATTRS)
      size = BM_FULL_SIZE (image->w, image->h);
    else
      size = BM_BITMAP_SIZE (image->w, image->h);
  } else {
    out_sf |= BMSF_ATTRS;
    out_flags |= BMF_ATTRS;
    size = BM_FULL_SIZE (image->w, image->h);
  }

  if (resolve_bitmap_params (
    &flags, &w, &h, out_sf, out_flags, size, out_w, out_h))
    goto error_exit;

  data = calloc (1, size);
  if (!data) {
    bitmap_error ("Memory allocation failed");
    goto error_exit;
  }

  /* line size in bytes of input image */
  ls0 = get_raw_image_size (image->colortype, image->bitdepth, image->w, 1);
  /* line size in bytes of output bitmap */
  ls1 = BM_BITMAP_SIZE (w, 1);
  /* width of bitmap in attributes */
  wa = w >> 3;
  /* height of bitmap in attributes */
  ha = h >> 3;
  /* start Y position */
  y0 = 0;

  /* attributes offset in output bitmap */
  if (flags & BMF_ATTRS)
    ao = BM_BITMAP_SIZE (w, h);
  else
    ao = 0; /* needed to remove "maybe-uninitialized" warning (GCC) */

  for (ya = 0; ya < ha; ya++) {
    unsigned x0, xa;

    /* start X position (multiple of attribute's width) */
    x0 = 0;

    for (xa = 0; xa < wa; xa++) {
      unsigned char pix[8*8]; /* converted pixels from input image */
      unsigned char *buf;
      unsigned char ncols;    /* number of colors in a block */
      unsigned char attr;     /* converted attribute for output bitmap */
      unsigned char ink, paper;
      uint32_t *p;
      unsigned x, y, cy;

      /* clear pixel buffer */
      memset (pix, 0, sizeof (pix));

      /* convert block of 8x8 pixels into pixel buffer */
      buf = pix;

      x = x0;
      y = y0;

      /* input image address */
      p = ((uint32_t *) image->data) + ((ls0 * y + (x << 2)) >> 2);

      for (cy = 0; cy < 8; cy++) {
        unsigned cx;
        for (cx = 0; cx < 8; cx++) {
          unsigned c;
          if (find_zx_color_RGBA8 (&c, *p)) {
            bitmap_error_fmt (
              "Invalid pixel color in input image at (%u,%u)",
              x, y);
            goto error_exit;
          }
          *buf = c;
          /* next pixel */
          buf++;
          p++;
          x++;
        }
        /* restore value */
        x = x0;
        /* next line */
        p += (ls0 >> 2) - 8;
        y++;
      }

      y = y0; /* restore value */

      /* convert block of 8x8 pixels into an attribute */
      if (make_zx_attribute (&ncols, &attr, pix, 8*8)) {
        bitmap_error_fmt (
          "Failed to set attribute for cell (%u,%u)-(%u,%u) (%s)",
          x, y, x + 8 - 1, y + 8 - 1, bitmap_error_text ());
        goto error_exit;
      }
      ink = attr & 7;
      paper = (attr >> 3) & 7;

      /* if we have external attributes - use them */
      if (attrs) {
        unsigned char nattr = attrs[wa * ya + xa];
        unsigned char nink = nattr & 7;
        unsigned char npaper = (nattr >> 3) & 7;
        bool ok = true;
        /* check "ink" color (must be provided attribute) */
        if (!((ink == nink) || (ink == npaper)))
          ok = false;
        /* check "paper" color (must be provided attribute) */
        if (ok && (ncols == 2) && (!((paper == nink) || (paper == npaper)))) {
          ok = false;
        }
        /* check brightness (must be the same, except for black background) */
        if (ok && (!((ncols == 1) && (ink == 0)))) {
          if ((attr ^ nattr) & (1<<6))
            ok = false;
        }
        if (!ok) {
          bitmap_error_fmt (
            "Provided attribute for cell (%u,%u)-(%u,%u) has wrong colors",
            x, y, x + 8 - 1, y + 8 - 1);
          goto error_exit;
        }
        /* use provided attribute */
        attr = nattr;
        ink = attr & 7;
        paper = (attr >> 3) & 7;

      } else if (ncols == 1 && ink) {
        /* swap set "paper" as "ink", set "ink" as black */
        attr = ((attr & 7) << 3) + ((attr >> 3) & 7) + (attr & 0xc0);
        ink = attr & 7;
        paper = (attr >> 3) & 7;
      }

      /* convert block of 8x8 pixels into a bitmap using an attribute */
      for (cy = 0; cy < 8; cy++) {
        unsigned char b = 0;  /* clear bits */
        unsigned o;

        /* Convert 8 horizontal pixels to a single byte */
        buf = pix + 8 * cy;
        if (ink != paper) {
          unsigned char m = 1<<7;
          unsigned cx;
          for (cx = 0; cx < 8; cx++) {
            if (((*buf) & 7) == ink) b |= m;  /* set bit */
            m >>= 1;
            buf++;
          }
        }

        /* Calculate destination offset */
        switch (flags & BMF_ADR_MASK) {
          case BMF_ADR_LINEAR: o = ls1 * y + xa; break;
          case BMF_ADR_SCREEN: o = SCR_ADDR (xa, y, ls1); break;
          case BMF_ADR_RCS: o = RCS_ADDR (xa, y); break;
          /* needed to remove "maybe-uninitialized" warning (GCC): */
          default: o = 0; break;
        }
        data[o] = b;

        y++;  /* next line */
      }

      /* save attribute */
      if (flags & BMF_ATTRS) {
        data[ao] = attr;
        ao++;
      }

      x0 += 8;  /* horizontal step */
    }

    y0 += 8;  /* vertical step */
  }

  bitmap_assign (self, flags, data, w, h);
  return 0;

error_exit:
  if (data) free (data);
  return -1;
}

/*
  From:   (0 0 0 S1 S0 C4 C3 C2 | C1 C0 R2 R1 R0 L2 L1 L0)
  To:     (0 1 0 S1 S0 L2 L1 L0 | R2 R1 R0 C4 C3 C2 C1 C0)
  Screen: (0 1 0 Y7 Y6 Y2 Y1 Y0 | Y5 Y4 Y3 X4 X3 X2 X1 X0)
  X:      (0 0 0 C4 C3 C2 C1 C0)
  Y:      (S1 S0 R2 R1 R0 L2 L1 L0)
*/
/* Returns 0 on success, -1 on error */
static int bitmap_convert_to_image_RGBA8 (struct bitmap_t *self,
  struct image_t *image, unsigned char *attrs) {

  unsigned w, h, ls, y;
  size_t size;
  unsigned char *data, *bm, *ba;
  uint32_t *p;
  bitmap_flags_t adr;

  /* image width in pixels */
  w = self->w;
  /* image height in pixels */
  h = self->h;
  /* image size in bytes */
  size = get_raw_image_size (ICT_RGBA, 8, w, h);

  data = calloc (1, size);
  if (!data) {
    bitmap_error ("Memory allocation failed");
    goto error_exit;
  }

  /* bitmap line size in bytes */
  ls = BM_BITMAP_SIZE (w, 1);
  /* input bitmap address mode */
  adr = self->flags & BMF_ADR_MASK;
  /* bitmap address */
  bm = self->data;
  /* the source of bitmap's attributes */
  if (attrs)
    ba = attrs;
  else if (self->flags & BMF_ATTRS)
    ba = bm + self->bitmap_size;
  else
    ba = NULL;

  /* current pixel address in image */
  p = (uint32_t *) data;

  for (y = 0; y < h; y++) {
    unsigned ao, bo, x;
    /* attribute offset */
    ao = ba ? ls * (y >> 3) : 0;
    /* byte offset in bitmap */
    switch (adr) {
      case BMF_ADR_LINEAR:
        bo = ls * y;
        break;
      case BMF_ADR_SCREEN:
        bo = SCR_ADDR (0, y, ls);
        break;
      default:
        bo = 0; /* needed to remove "maybe-uninitialized" warning (GCC) */
        break;
    }
    for (x = 0; x < ls; x++) {
      unsigned char b, i;
      uint32_t c0, c1;
      switch (adr) {
        case BMF_ADR_RCS:
          bo = RCS_ADDR (x, y);
          break;
        default:
          break;
      }
      if (ba) {
        unsigned char a, ci0, ci1;
        /* current attribute in bitmap */
        a = ba[ao];
        /* color index for bit 0 */
        ci0 = (a >> 3) & 7;
        /* color index for bit 1 */
        ci1 = a & 7;
        if (a & 0x40) { ci0 += 8; ci1 += 8; };
        c0 = ZXPALETTE[ci0];
        c1 = ZXPALETTE[ci1];
      } else {
        c0 = ZXPALETTE[DEF_PAPER_COL];
        c1 = ZXPALETTE[DEF_INK_COL];
      }
      /* current byte in bitmap */
      b = bm[bo];
      for (i = 0; i < 8; i++) {
        *p = (b & 0x80) ? c1 : c0;
        p++;
        b <<= 1;
      }
      if (ba) ao++;
      switch (adr) {
        case BMF_ADR_LINEAR:
        case BMF_ADR_SCREEN:
          bo++;
          break;
        default:
          break;
      }
    }
  }

  image_assign_raw (image, ICT_RGBA, 8, data, size, w, h);
  return 0;

error_exit:
  if (data) free (data);
  return -1;
}

/* Returns 0 on success, -1 on error */
int bitmap_save_as (struct bitmap_t *self, const char *filename) {
  int status = file_save (self->data, self->size, filename);
  if (status) bitmap_error ("Failed to save output file");
  return status;
}

/* Returns 0 on success, other value on error. */
int bitmap_save_attributes_as (struct bitmap_t *self, const char *filename) {

  if (!((self->flags & BMF_ATTRS) && self->data && self->attrs_size)) {
    bitmap_error ("Bitmap has no attributes to save");
    return -1;
  }

  return file_save (self->data + self->bitmap_size, self->attrs_size,
    filename);
}


/* Returns 0 on success, -1 on error, 1 if data is not a bitmap. */
int import_bitmap_from_memory (struct bitmap_t **self,
  bitmap_setup_flags_t sf, bitmap_flags_t flags, unsigned char *data,
  size_t size, unsigned w, unsigned h) {

  int status; /* exit status */
  struct bitmap_t *bitmap = NULL;
  bitmap_flags_t bm_flags;
  unsigned char *bm_data = NULL;
  unsigned bm_w, bm_h;

  status = resolve_bitmap_params (
    &bm_flags, &bm_w, &bm_h, sf, flags, size, w, h);
  if (status) goto error_exit;

  bitmap = bitmap_new ();
  if (!bitmap) { status = -1; goto error_exit; }

  bm_data = malloc (size);
  if (!bm_data) {
    bitmap_error ("Memory allocation failed");
    status = -1; goto error_exit;
  }
  memcpy (bm_data, data, size);
  bitmap_assign (bitmap, bm_flags, bm_data, bm_w, bm_h);
  *self = bitmap;
  return 0;

error_exit:
  if (bitmap) bitmap_dispose (&bitmap);
  if (bm_data) free (bm_data);
  return status;
}

/* Returns 0 on success, -1 on error, 1 if file is not a bitmap. */
int import_bitmap_from_file (struct bitmap_t **self, const char *filename,
  bitmap_setup_flags_t sf, bitmap_flags_t flags, unsigned w, unsigned h) {

  int status; /* exit status */
  unsigned char *data = NULL;
  size_t size;
  struct bitmap_t *bitmap = NULL;

  status = file_load (&data, &size, filename);
  if (status) {
    bitmap_error ("Failed to load input file");
    goto error_exit;
  }

  status = import_bitmap_from_memory (&bitmap, sf, flags, data, size, w, h);
  if (status) goto error_exit;

  free (data);

  status = bitmap_set_filename (bitmap, filename);
  if (status) goto error_exit;

  *self = bitmap;
  return 0;

error_exit:
  if (data) free (data);
  if (bitmap) bitmap_dispose (&bitmap);
  return status;
}

/* Returns 0 on success, other value on error. */
int import_bitmap_from_image (struct bitmap_t **self,
  bitmap_setup_flags_t sf, bitmap_flags_t flags, unsigned w, unsigned h,
  struct image_t *image, unsigned char *attrs) {

  int status; /* exit status */
  struct bitmap_t *bitmap = NULL;

  bitmap = bitmap_new ();
  if (!bitmap) { status = -1; goto error_exit; }

  if ((image->type == IMGT_RAW)
  &&  (image->colortype == ICT_RGBA)
  &&  (image->bitdepth == 8))  {
    status = bitmap_convert_from_image_RGBA8 (
      bitmap, sf, flags, w, h, image, attrs);
    if (status) goto error_exit;
  } else {
    bitmap_error ("Unsupported input image format");
    status = -1;
    goto error_exit;
  }

  *self = bitmap;
  return 0;

error_exit:
  if (bitmap) bitmap_dispose (&bitmap);
  return status;
}

struct bitmap_t *export_bitmap_as_bitmap (struct bitmap_t *self,
  bitmap_flags_t flags, unsigned w, unsigned h, unsigned char *attrs) {

  struct bitmap_t *bitmap = bitmap_new ();
  if (!bitmap) {
    bitmap_error ("Memory allocation failed");
    goto error_exit;
  }

  if (bitmap_convert_from_bitmap (bitmap, flags, w, h, self, attrs))
    goto error_exit;

  return bitmap;

error_exit:
  if (bitmap) bitmap_dispose (&bitmap);
  return NULL;
}

struct image_t *export_bitmap_as_image (struct bitmap_t *self,
  enum image_color_type_t colortype, unsigned bitdepth,
  unsigned char *attrs) {

  struct image_t *image = image_new ();
  if (!image) {
    bitmap_error (image_error_text ());
    goto error_exit;
  }

  if ((colortype == ICT_RGBA) && (bitdepth == 8)) {
    if (bitmap_convert_to_image_RGBA8 (self, image, attrs)) goto error_exit;
  } else {
    bitmap_error ("Unsupported output image format");
    goto error_exit;
  }

  return image;

error_exit:
  if (image) image_dispose (&image);
  return NULL;
}
