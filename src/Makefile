#!/bin/make -f
#
# Supported environments:
#   * GNU on Linux, FreeBSD etc.
#   * GNU on Windows NT (using MinGW/MSYS/Cygwin/WSL)
#
# Build:
#   make [BUILD=<BUILD>] [<TARGET> ...]
# Install / Uninstall:
#   make [BUILD=<BUILD>] [prefix=<PREFIX>] install | uninstall
# Clean:
#   make [BUILD=<BUILD>] clean
#   make distclean
#
# where:
#   <BUILD> - see included `common.mak'.
#   <TARGET> is one of values for `BINS' variable prefixed with "$(builddir)/"
#     (see target `all' below).
#   <PREFIX> is a prefix directory to install files into.
#
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: Copyright (C) 2021-2024 Ivan Tatarinov
# SPDX-License-Identifier: GPL-3.0-or-later

include ../common.mak

# 0 or 1
STATIC_BUILD ?= 1

srcdir		= .
ifeq ($(STATIC_BUILD),0)
 builddir	?= build
else
 builddir	?= build/static
endif
prefix		?= /usr/local
exec_prefix	?= $(prefix)
bindir		?= $(exec_prefix)/bin

INCLUDEDIR	?= $(srcdir)
LIBDIR		?= $(builddir)

LODEPNG_DEPS = $(INCLUDEDIR)/lodepng.h $(INCLUDEDIR)/lodepng_ver.h
ifeq ($(STATIC_BUILD),0)
 LODEPNG_DEPS += $(LIBDIR)/liblodepng$(DLLSUFFIX)
else
 LODEPNG_DEPS += $(INCLUDEDIR)/lodepng.c
endif

BINS =\
 fontconv$(EXESUFFIX)\
 scrconv$(EXESUFFIX)

BIN_CFLAGS = -fPIC -std=c99 -Wall -Wextra -Wpedantic -Wno-unused-function -O3
BIN_CFLAGS += $(addprefix -I,$(INCLUDEDIR))
BIN_CFLAGS += -DSTATIC_BUILD=$(STATIC_BUILD)
ifeq ($(STATIC_BUILD),0)
 BIN_CFLAGS += $(addprefix -L,$(LIBDIR))
endif

.PHONY: all
all: $(addprefix $(builddir)/,$(BINS))

$(builddir) \
$(DESTDIR)$(bindir):
	mkdir -p $@

$(builddir)/fontconv$(EXESUFFIX): $(srcdir)/fontconv.c\
 $(LODEPNG_DEPS)\
 $(srcdir)/common.c\
 $(srcdir)/common.h\
 $(srcdir)/errors.c\
 $(srcdir)/errors.h\
 $(srcdir)/font.c\
 $(srcdir)/font.h\
 $(srcdir)/image.c\
 $(srcdir)/image.h\
 $(srcdir)/palette.c\
 $(srcdir)/palette.h\
 Makefile | $(builddir)
ifeq ($(STATIC_BUILD),0)
# Fix for the GNU linker (otherwise it will fail to find the library)
# Update the link
	ln -f $(LIBDIR)/liblodepng$(DLLSUFFIX)\
	 $(srcdir)/liblodepng$(DLLSUFFIX)
	$(CC) $(BIN_CFLAGS) $(CFLAGS) -o $@ $<\
	 $(foreach l,liblodepng$(DLLSUFFIX),-l:$(l) $(l))
else
# Remove the link
	$(RM) $(srcdir)/liblodepng$(DLLSUFFIX)
	$(CC) $(BIN_CFLAGS) $(CFLAGS) -o $@ $<
endif

$(builddir)/scrconv$(EXESUFFIX): $(srcdir)/scrconv.c\
 $(LODEPNG_DEPS)\
 $(srcdir)/bitmap.c\
 $(srcdir)/bitmap.h\
 $(srcdir)/common.c\
 $(srcdir)/common.h\
 $(srcdir)/errors.c\
 $(srcdir)/errors.h\
 $(srcdir)/image.c\
 $(srcdir)/image.h\
 $(srcdir)/palette.c\
 $(srcdir)/palette.h\
 Makefile | $(builddir)
ifeq ($(STATIC_BUILD),0)
# Fix for the GNU linker (otherwise it will fail to find the library)
# Update the link
	ln -f $(LIBDIR)/liblodepng$(DLLSUFFIX)\
	 $(srcdir)/liblodepng$(DLLSUFFIX)
	$(CC) $(BIN_CFLAGS) $(CFLAGS) -o $@ $<\
	 $(foreach l,liblodepng$(DLLSUFFIX),-l:$(l) $(l))
else
# Remove the link
	$(RM) $(srcdir)/liblodepng$(DLLSUFFIX)
	$(CC) $(BIN_CFLAGS) $(CFLAGS) -o $@ $<
endif

# $1 = filename
define install_bin_rule =
$$(DESTDIR)$$(bindir)/$1: $$(builddir)/$1 | $$(DESTDIR)$$(bindir)
	$$(INSTALL_PROGRAM) $$< $$@
endef

$(foreach f,$(BINS),$(eval $(call install_bin_rule,$(f))))

.PHONY: install
install: $(addprefix $(DESTDIR)$(bindir)/,$(BINS)) | $(DESTDIR)$(bindir)

.PHONY: install-strip
install-strip:
	$(MAKE) -w INSTALL_PROGRAM='$(INSTALL_PROGRAM) -s' install

.PHONY: uninstall
uninstall:
	$(RM) $(addprefix $(DESTDIR)$(bindir)/,$(BINS))

.PHONY: clean
clean:
	$(RM) $(srcdir)/liblodepng$(DLLSUFFIX)
	$(RM) $(addprefix $(builddir)/,$(BINS))

.PHONY: distclean
distclean:
	$(RM)\
	 $(srcdir)/liblodepng.so\
	 $(srcdir)/liblodepng.dll
	$(RM) -r $(builddir)
