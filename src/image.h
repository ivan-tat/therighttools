/*
 * image.h - header file for "image.c".
 *
 * Copyright (C) 2021 Ivan Tatarinov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-FileType: SOURCE
 * SPDX-FileCopyrightText: Copyright (C) 2021 Ivan Tatarinov
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _IMAGE_H_INCLUDED
#define _IMAGE_H_INCLUDED

/* Type of image */
typedef unsigned image_type_t;
#define IMGT_RAW 0
#define IMGT_PNG 1

/* Color type of image */
typedef enum image_color_type_t {
  ICT_GREY = 0,
  ICT_RGBA = 1
} ImageColorType;

struct image_t {
  image_type_t type;
  enum image_color_type_t colortype;  /* image color type */
  unsigned bitdepth;    /* image color bit depth */
  unsigned char *data;  /* image data */
  size_t size;  /* image data size */
  unsigned w;   /* image width */
  unsigned h;   /* image height */
  char *filename;
};

struct image_format_lodepng_t {
  enum LodePNGColorType colortype;
  unsigned bitdepth;
};

/* Get an error message of a last failed image-routine */
const char *image_error_text ();

const char *get_image_type_str (image_type_t type);

size_t get_raw_image_size (enum image_color_type_t ct, unsigned bd,
  unsigned w, unsigned h);

struct image_t *image_new ();
struct image_t *image_dup (struct image_t *self);
void image_free (struct image_t *self);
void image_dispose (struct image_t **self);
void image_assign_raw (struct image_t *self, enum image_color_type_t ct,
  unsigned bd, unsigned char *data, size_t size, unsigned w, unsigned h);
void image_assign_png (struct image_t *self, unsigned char *data,
  size_t size, unsigned w, unsigned h);
int image_set_filename (struct image_t *self, const char *filename);
int image_format_to_lodepng (struct image_t *self,
  struct image_format_lodepng_t *out);
int image_convert_to_png (struct image_t *self, struct image_t *image);
int image_save_as (struct image_t *self, const char *filename);

int import_image_from_memory_png (struct image_t **self,
  enum image_color_type_t ct, unsigned bd, unsigned char *data, size_t size);
int import_image_from_file_png (struct image_t **self,
  enum image_color_type_t ct, unsigned bd, const char *filename);

struct image_t *export_image_as_image_png (struct image_t *self);

#endif  /* !_IMAGE_H_INCLUDED */
